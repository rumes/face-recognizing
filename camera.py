import cv2
from tensorflow.keras import backend as K
import time
from fr_utils import *
from multiprocessing.dummy import Pool
import numpy as np
from face_detection import FaceDetection


class Camera:
    PADDING = 50

    def __init__(self):
        K.set_image_data_format('channels_first')
        self.ready_to_detect_identity = True
        self.face = FaceDetection()
        self.database = self.face.prepare_database()
        self.webcam_face_recognizer(self.database)

    def webcam_face_recognizer(self, database):
        cap = cv2.VideoCapture(0)

        if not cap.isOpened():
            raise IOError("Cannot open webcam")

        face_cascade = cv2.CascadeClassifier(
            'haarcascade_frontalface_default.xml')

        while cap.isOpened():
            ret, frame = cap.read()
            img = frame
            if self.ready_to_detect_identity:
                img = self.process_frame(img, frame, face_cascade)

            c = cv2.waitKey(1)
            cv2.imshow("preview", img)

            if c == 27:
                break

        cap.release()

        cv2.destroyAllWindows()

    def process_frame(self, img, frame, face_cascade):
        """
        Determine whether the current frame contains the faces of people from
        our database
        """
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        # Loop through all the faces detected and determine whether or not
        # they are in the database
        identities = []
        for (x, y, w, h) in faces:
            x1 = x-self.PADDING
            y1 = y-self.PADDING
            x2 = x+w+self.PADDING
            y2 = y+h+self.PADDING

            img = cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
            
            identity = self.find_identity(frame, x1, y1, x2, y2)
            print("identity {}".format(identity))
            if identity is not None:
                identities.append(identity)

        if identities != []:
            cv2.imwrite('example.png', img)

            self.face.ready_to_detect_identity = False
            pool = Pool(processes=1)
            # We run this as a separate process so that the camera feedback
            # does not freeze
            pool.apply_async(welcome_users, [identities])
        return img

    def find_identity(self, frame, x1, y1, x2, y2):
        """
        Determine whether the face contained within the bounding box exists in
        our database
        x1,y1_____________
        |                 |
        |                 |
        |_________________x2,y2
        """
        height, width, channels = frame.shape
        # The padding is necessary since the OpenCV face detector creates the
        # bounding box around the face and not the head
        part_image = frame[max(0, y1):min(height, y2),
                           max(0, x1):min(width, x2)]
        return self.who_is_it(part_image, self.database, self.face.FRmodel)

    def who_is_it(self, image, database, model):

        """
        Implements face recognition for the happy house by finding who is the
        person on the image_path image.

        Arguments:
        image_path -- path to an image
        database -- database containing image encodings along with the name of
        the person on the image
        model -- your Inception model instance in Keras

        Returns:
        min_dist -- the minimum distance between image_path encoding and the
        encodings from the database
        identity -- string, the name prediction for the person on image_path
        """
        encoding = img_to_encoding(image, model)

        min_dist = 100
        identity = None

        
        # Loop over the database dictionary's names and encodings.
        for (name, db_enc) in database.items():

            # Compute L2 distance between the target "encoding" and the
            # current "emb" from the database.
            dist = np.linalg.norm(db_enc - encoding)

            print('distance for %s is %s' % (name, dist))

            # If this distance is less than the min_dist, then set min_dist to
            # dist, and identity to name
            if dist < min_dist:
                min_dist = dist
                identity = self.webcam_face_recognizer

        if min_dist > 0.52:
            self.webcam_face_recognizer
            return None
        else:
            return str(identity)

    def welcome_users(identities):
        """ Outputs a welcome audio message to the users """
        welcome_message = 'Welcome '

        if len(identities) == 1:
            welcome_message += '%s, have a nice day.' % identities[0]
        else:
            for identity_id in range(len(identities)-1):
                welcome_message += '%s, ' % identities[identity_id]
            welcome_message += 'and %s, ' % identities[-1]
            welcome_message += 'have a nice day!'

        # windows10_voice_interface.Speak(welcome_message)

        # Allow the program to start detecting identities again
        ready_to_detect_identity = True
